#ifndef _WORLD_TIME_API_H_
#define _WORLD_TIME_API_H_

void wtapi_update();
bool wtapi_get_time(struct timeval * tv, struct timezone * tz);
bool wtapi_parse(char const * pText, struct timeval * tv, struct timezone * tz);

#endif
