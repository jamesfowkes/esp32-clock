/* C/C++ Includes */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>

/* Debugging */

#ifdef UNIT_TEST
#include <iostream>
#endif

/* Application Includes */

#include "wtapi.h"

static bool parse_int(char const * p_start, int32_t * p_result)
{
    char * p_local_end;
    int32_t local = strtol(p_start, &p_local_end, 10);

    if (p_local_end > p_start)
    {
        *p_result = local;
    }
    return p_local_end > p_start;
}

static int32_t get_dst_offset(char const * p_text)
{
    char * found = strstr((char*)p_text, "dst_offset: ");
    int32_t dst = 0U;
    if (found)
    {
        parse_int(found + 12, &dst);
    }

    return dst;
}

static int32_t get_unix_time(char const * p_text)
{
    int32_t result = 0;
    char * found = strstr((char*)p_text, "unixtime: ");

    if (found)
    {
        parse_int(found + 10, &result);
    }
    return result;
}

bool wtapi_parse(char const * p_text, struct timeval * tv, struct timezone * tz)
{
    int32_t dst_offset = get_dst_offset(p_text);
    int32_t unix_time = get_unix_time(p_text);

    tv->tv_sec = time_t(unix_time);
    tz->tz_minuteswest = 0;
    tz->tz_dsttime = dst_offset;
    return true;
}

#ifdef UNIT_TEST

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

std::string slurp(std::ifstream& in) {
    std::stringstream sstr;
    sstr << in.rdbuf();
    return sstr.str();
}

class WorldTimeAPITest : public CppUnit::TestFixture  {
    CPPUNIT_TEST_SUITE(WorldTimeAPITest);

    CPPUNIT_TEST(test_parsing_dst);
    CPPUNIT_TEST(test_parsing_nondst);

    CPPUNIT_TEST_SUITE_END();

    void test_parsing_dst()
    {
        char const * p_text;
        std::string text_string;
        std::ifstream test_file ("dst-test.txt");
        if (test_file.is_open())
        {
            text_string = slurp(test_file);
            p_text = text_string.c_str();
        }
        test_file.close();

        timeval tv;
        struct timezone tz;

        wtapi_parse(p_text, &tv, &tz);

        CPPUNIT_ASSERT_EQUAL(time_t(1586986631), tv.tv_sec);
        CPPUNIT_ASSERT_EQUAL(0, tz.tz_minuteswest);
        CPPUNIT_ASSERT_EQUAL(3600, tz.tz_dsttime);
    }

    void test_parsing_nondst()
    {
        char const * p_text;
        std::string text_string;
        std::ifstream test_file ("non-dst-test.txt");
        if (test_file.is_open())
        {
            text_string = slurp(test_file);
            p_text = text_string.c_str();
        }
        test_file.close();

        timeval tv;
        struct timezone tz;

        wtapi_parse(p_text, &tv, &tz);

        CPPUNIT_ASSERT_EQUAL(time_t(1581807145), tv.tv_sec);
        CPPUNIT_ASSERT_EQUAL(0, tz.tz_minuteswest);
        CPPUNIT_ASSERT_EQUAL(0, tz.tz_dsttime);
    }
};

int main()
{
   CppUnit::TextUi::TestRunner runner;
   
   CPPUNIT_TEST_SUITE_REGISTRATION( WorldTimeAPITest );

   CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();

   runner.addTest( registry.makeTest() );
   runner.run();

   return 0;
}

#endif
