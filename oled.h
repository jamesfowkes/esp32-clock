#ifndef _OLED_H_
#define _OLED_H_

void oled_setup();
void oled_loop(struct tm& timeinfo);

#endif
