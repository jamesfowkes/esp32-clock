/* C/C++ Includes */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>

/* Arduino Includes */

#include <Arduino.h>
#include "fixed-length-accumulator.h"

/* Application Includes */

#include "wtapi.h"
#include "http-handler.h"

static bool s_downloading = false;
static const uint16_t MAX_TEXT_SIZE = 4096;
static char s_text_buffer[MAX_TEXT_SIZE];
static FixedLengthAccumulator s_text_accumulator(s_text_buffer, MAX_TEXT_SIZE);

static char WORLD_TIME_TEXT_API_EU_LONDON[] = "http://worldtimeapi.org/api/timezone/Europe/London.txt";

void wtapi_update()
{
    if (!s_downloading)
    {
        s_text_accumulator.reset();
        s_downloading = http_start_download(WORLD_TIME_TEXT_API_EU_LONDON);
    }
}

bool wtapi_get_time(timeval * tv, timezone * tz)
{
    bool ok = false;
    if (s_downloading)
    {
        if (http_handle_get_stream(s_text_accumulator))
        {
            s_downloading = false;
            ok = wtapi_parse(s_text_accumulator.c_str(), tv, tz);
            if (ok)
            {
                Serial.print("Got time ");
                Serial.print(tv->tv_sec);
                Serial.print(" DST offset ");
                Serial.println(tz->tz_dsttime);
            }
            else
            {
                Serial.print("Could not parse WTAPI text");
            }
        }
    }
    return ok;
}
