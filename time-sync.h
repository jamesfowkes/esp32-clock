#ifndef _TIME_SYNC_H_
#define _TIME_SYNC_H_

void time_sync_loop();
bool time_sync_get_time(struct tm& timeinfo);

#endif

