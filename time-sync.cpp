/* C/C++ Includes */

#include <stdint.h>
#include <time.h>

/* Platform Includes */

#include <WiFi.h>

/* Arduino Includes */

#include <TaskAction.h>
#include "fixed-length-accumulator.h"

/* Application Includes */

#include "wtapi.h" 
#include "time-sync.h" 
#include "http-handler.h"

/* Local Variables */

static uint32_t last_sync = UINT32_MAX;
static struct tm s_timeinfo;

static void update_time_task_fn(TaskAction * pTask)
{
    if (time_sync_get_time(s_timeinfo))
    {
        Serial.println(&s_timeinfo, "%A, %B %d %Y %H:%M:%S");
    }
    else
    {
        Serial.println("No time established");
    }
}
static TaskAction s_time_sync_task(update_time_task_fn, 1000, INFINITE_TICKS);

bool time_sync_get_time(struct tm& timeinfo)
{
    if(WiFi.status() == WL_CONNECTED)
    {
        if ((last_sync == UINT32_MAX) || ((millis() - last_sync) > (86400 * 1000)))
        {
            last_sync = millis();
            Serial.println("Trying time sync");

            wtapi_update();

            timeval tv;
            timezone tz;
            timezone tz_utc = {0,0};
            if (wtapi_get_time(&tv, &tz))
            {
                tv.tv_sec += tz.tz_dsttime;
                settimeofday(&tv, &tz_utc);
            }
        }
    }

    time_t now = time(nullptr);
    gmtime_r(&now, &timeinfo);

    return (now > 8 * 3600 * 2);
}

void time_sync_loop()
{
    http_handler_loop();
    s_time_sync_task.tick();
}
