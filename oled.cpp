#include <time.h>

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 32
#define OLED_RESET    -1

Adafruit_SSD1306 s_display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

static bool draw_flash(unsigned long now, bool sync=false)
{
    static unsigned long next_flash = 100;

    static uint32_t height = 1;
    static const uint32_t MAX_HEIGHT = 10;
    static const uint32_t WIDTH = 10;
    static const uint32_t X = SCREEN_WIDTH - WIDTH - 6;
    static const uint32_t Y = SCREEN_HEIGHT;
    static uint32_t colour = SSD1306_WHITE;

    bool bDirty = false;

    if ((now >= next_flash) || sync)
    {
        height = sync ? 1 : height + 1;
        s_display.fillRect(X, Y-height, WIDTH, height, colour);
        next_flash = now + 100;
        bDirty = true;
    }    
    
    colour = SSD1306_INVERSE;

    return bDirty;
}

static bool draw_time(struct tm& timeinfo)
{
    static struct tm last_time = {0};
    char formattedDate[16];
    char formattedTime[10];
    bool bDirty = false;

    if (memcmp(&last_time, &timeinfo, sizeof(struct tm)))
    {
        memcpy(&last_time, &timeinfo, sizeof(struct tm));
        sprintf(formattedDate, "%04d-%02d-%02d", timeinfo.tm_year+1900, timeinfo.tm_mon, timeinfo.tm_mday);
        sprintf(formattedTime, "%02d:%02d:%02d", timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);

        s_display.clearDisplay();
        s_display.setCursor(2, 2);
        s_display.println(formattedDate);
        s_display.print(formattedTime);
        draw_flash(0, true);
        bDirty = true;
    }
    return bDirty;
}

void oled_setup()
{
    if (!s_display.begin(SSD1306_SWITCHCAPVCC, 0x3C))
    {
        Serial.println(F("SSD1306 allocation failed"));
    }

    s_display.setTextSize(2);
    s_display.setTextColor(SSD1306_WHITE);
    s_display.cp437(true);
    s_display.clearDisplay();
}

void oled_loop(struct tm& timeinfo)
{
    bool update = false;
    update |= draw_flash(millis());
    update |= draw_time(timeinfo);
    if (update)
    {
        s_display.display();
    }
}