/* Platform Includes */

#include <Esp.h>
#include <esp_system.h>
#include <TaskAction.h>
#include <WiFi.h>

/* Application Includes */

#include "time-sync.h" 
#include "oled.h" 
#include "app-wifi.h"

const char *ssid     = "Night Vale Library";
const char *password = "CecilIsLot37";

static hw_timer_t *timer = NULL;

void IRAM_ATTR resetModule() {
    esp_restart();
}

void setup()
{

    Serial.begin(115200);
    Serial.setDebugOutput(true);
    esp_log_level_set("*", ESP_LOG_VERBOSE);

    app_wifi_setup();

    oled_setup();

    timer = timerBegin(0, 80, true);
    timerAttachInterrupt(timer, &resetModule, true);
    timerAlarmWrite(timer, 20000 * 1000, false);
    timerAlarmEnable(timer);
}

void loop()
{
    struct tm timeinfo;
    time_sync_loop();
    time_sync_get_time(timeinfo);
    oled_loop(timeinfo);
    timerWrite(timer, 0);
}